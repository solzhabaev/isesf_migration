﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirAstana.ISESF.Core.Models
{
    /// <summary>
    /// Счёт-фактура
    /// </summary>
    public class Invoice
    {
        public int Id { get; set; }

        public State State { get; set; }

        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
    }
}

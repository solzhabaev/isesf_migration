﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AirAstana.ISESF.Core.Models
{
    /// <summary>
    /// Пользователи
    /// </summary>
    public class ApplicationUser: IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Здесь добавьте настраиваемые утверждения пользователя
            return userIdentity;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IIN { get; set; }

        public DateTime? ElectronicKeyFinishedDate { get; set; }

        public PageActionEnum ImportActionInvoicseAction { get; set; }

        public PageActionEnum ExportActionInvoicseAction { get; set; }

        public PageActionEnum ApplicationUsersAction { get; set; }

        public PageActionEnum ReportsAction { get; set; }

        public State State { get; set; }
    }
}

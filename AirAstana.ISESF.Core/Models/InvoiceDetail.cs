﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirAstana.ISESF.Core.Models
{
    /// <summary>
    /// Счёт-фактура строки
    /// </summary>
    public class InvoiceDetail
    {
        public int Id { get; set; }

        public int InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        public State State { get; set; }
    }
}

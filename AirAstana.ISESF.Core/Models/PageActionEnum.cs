﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirAstana.ISESF.Core.Models
{
    /// <summary>
    /// Действия на странице
    /// </summary>
    public enum PageActionEnum
    {
        /// <summary>
        /// Доступ закрыт
        /// </summary>
        Denied = 1,
        /// <summary>
        /// Просмотр
        /// </summary>
        Read = 2,
        /// <summary>
        /// Изменения
        /// </summary>
        Write = 3
    }
}

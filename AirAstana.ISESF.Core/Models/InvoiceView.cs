﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirAstana.ISESF.Core.Models
{
    /// <summary>
    /// View счёт-фактур, из внутренней системы
    /// </summary>
    public class InvoiceView
    {
        public int Id { get; set; }

        public string InvoiceSet_invoiceInfo_certificate { get; set; }

        public DateTime InvoiceSet_invoiceInfo_inputDate { get; set; }

        public int InvoiceSet_invoiceInfo_invoiceId { get; set; }

        public int InvoiceSet_invoiceInfo_invoiceId_agg { get; set; }

        public string InvoiceSet_invoiceInfo_invoiceStatus { get; set; }

        public DateTime InvoiceSet_invoiceInfo_lastUpdateDate { get; set; }

        public bool InvoiceSet_invoiceInfo_oldSignature { get; set; }

        public string InvoiceSet_invoiceInfo_registrationNumber { get; set; }

        public bool InvoiceSet_invoiceInfo_signatureValid { get; set; }

        public string Invoice_addInf { get; set; }

        public string Invoice_consignee_address { get; set; }

        public string Invoice_consignee_name { get; set; }

        public int Invoice_consignee_tin { get; set; }

        public int Invoice_consignee_tin_agg { get; set; }

        public string Invoice_consignor_address { get; set; }

        public string Invoice_consignor_name { get; set; }

        public int Invoice_consignor_tin { get; set; }

        public int Invoice_consignor_tin_agg { get; set; }

        public string Invoice_customers_customer_address { get; set; }

        public string Invoice_customers_customer_name { get; set; }

        public int Invoice_customers_customer_tin { get; set; }

        public int Invoice_customers_customer_tin_agg { get; set; }

        public string Invoice_customers_customer_trailer { get; set; }

        public DateTime Invoice_date { get; set; }

        public DateTime Invoice_deliveryTerm_contractDate { get; set; }

        public string Invoice_deliveryTerm_contractNum { get; set; }

        public string Invoice_deliveryTerm_contractNum_agg { get; set; }

        public string Invoice_deliveryTerm_destination { get; set; }

        public string Invoice_deliveryTerm_exerciseWay { get; set; }

        public string Invoice_deliveryTerm_term { get; set; }

        public string Invoice_deliveryTerm_warrant { get; set; }

        public string Invoice_deliveryTerm_warrant_agg { get; set; }

        public DateTime Invoice_deliveryTerm_warrantDate { get; set; }

        public string Invoice_invoiceType { get; set; }

        public string Invoice_num { get; set; }

        public string Invoice_num_agg { get; set; }

        public string Invoice_operatorFullname { get; set; }

        public string Invoice_productSet_currencyCode { get; set; }

        public int Invoice_productSet_product_description { get; set; }

        public int Invoice_productSet_product_ndsAmount { get; set; }

        public int Invoice_productSet_product_ndsAmount_agg { get; set; }

        public int Invoice_productSet_product_ndsRate { get; set; }

        public int Invoice_productSet_product_ndsRate_agg { get; set; }

        public int Invoice_productSet_product_priceWithoutTax { get; set; }

        public int Invoice_productSet_product_priceWithoutTax_agg { get; set; }

        public int Invoice_productSet_product_priceWithTax { get; set; }

        public int Invoice_productSet_product_priceWithTax_agg { get; set; }

        public int Invoice_productSet_product_quantity { get; set; }

        public int Invoice_productSet_product_quantity_agg { get; set; }

        public int Invoice_productSet_product_turnoverSize { get; set; }

        public int Invoice_productSet_product_turnoverSize_agg { get; set; }

        public string Invoice_productSet_product_unitNomenclature { get; set; }

        public int Invoice_productSet_product_unitPrice { get; set; }

        public int Invoice_productSet_product_unitPrice_agg { get; set; }

        public int Invoice_productSet_totalExciseAmount { get; set; }

        public int Invoice_productSet_totalExciseAmount_agg { get; set; }

        public int Invoice_productSet_totalNdsAmount { get; set; }

        public int Invoice_productSet_totalNdsAmount_agg { get; set; }

        public int Invoice_productSet_totalPriceWithoutTax { get; set; }

        public int Invoice_productSet_totalPriceWithoutTax_agg { get; set; }

        public int Invoice_productSet_totalPriceWithTax { get; set; }

        public int Invoice_productSet_totalPriceWithTax_agg { get; set; }

        public int Invoice_productSet_totalTurnoverSize { get; set; }

        public int Invoice_productSet_totalTurnoverSize_agg { get; set; }

        public string Invoice_sellers_seller_address { get; set; }

        public string Invoice_sellers_seller_bank { get; set; }

        public int Invoice_sellers_seller_bik { get; set; }

        public int Invoice_sellers_seller_bik_agg { get; set; }

        public int Invoice_sellers_seller_certificateNum { get; set; }

        public int Invoice_sellers_seller_certificateNum_agg { get; set; }

        public int Invoice_sellers_seller_certificateSeries { get; set; }

        public int Invoice_sellers_seller_certificateSeries_agg { get; set; }

        public DateTime Invoice_sellers_seller_deliveryDocDate { get; set; }

        public string Invoice_sellers_seller_deliveryDocNum { get; set; }

        public string Invoice_sellers_seller_deliveryDocNum_agg { get; set; }

        public int Invoice_sellers_seller_iik { get; set; }

        public int Invoice_sellers_seller_iik_agg { get; set; }

        public int Invoice_sellers_seller_kbe { get; set; }

        public int Invoice_sellers_seller_kbe_agg { get; set; }

        public string Invoice_sellers_seller_name { get; set; }

        public int Invoice_sellers_seller_tin { get; set; }

        public int Invoice_sellers_seller_tin_agg { get; set; }

        public bool IsFinished { get; set; }
    }
}
